package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)
// this is a change I'm making for assignemnt 14


public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }
   //Anthony's Test
   @Test
   @DisplayName("Test for Anthony")
   public void testGreeterAnthony() 

   {
      g.setName("Anthony");
      assertEquals(g.getName(),"Anthony");
      assertEquals(g.sayHello(),"Hello Anthony!");
   }


// Edited portion for assignment 14
 @Test
   @DisplayName("Test for Aaron")
   public void testGreeterCasper() 

   {
      g.setName("Aaron");
      assertEquals(g.getName(),"Aaron");
      assertEquals(g.sayHello(),"Hello Aaron!");
   }

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

      @Test
   @DisplayName("Test for Erick")
   public void testGreeterErick() 

   {
      g.setName("Erick");
      assertEquals(g.getName(),"Erick");
      assertEquals(g.sayHello(),"Hello Erick!");
   }

}
